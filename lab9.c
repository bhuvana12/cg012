
#include<stdio.h>
void swapnum(int *var1,int *var2)
{
    int tempnum;
    tempnum=*var1;
    *var1=*var2;
    *var2=tempnum;
}
int main()
{

int num1=35, num2=45;
printf("before swapping:");
printf("\n num1 value is %d",num1);
printf("\n num1 value is %d",num2);

swapnum(&num1,&num2);
printf("\n after swapping\n");
printf("the value of num1 is %d\n",num1);
printf("value of num2 is %d\n",num2);
return 0;
}

